using System.Threading.Tasks;
using HelloWorld.Interfaces;
using Orleans;
using Orleans.Providers;
using SharedLogic;

namespace HelloWorld.Grains
{
    public class GameState
    {
        public string Player1;
        public string Player2;
        public TicTacToe.State  State;
    }
    
    [StorageProvider(ProviderName = "profileStore")]
    public class Game : Grain<GameState>, IGame
    {
        public override Task OnActivateAsync()
        {
            State.State ??= new TicTacToe.State();
            return base.OnActivateAsync();
        }

        Task IGame.Setup(string player1, string player2)
        {
            if (!string.IsNullOrEmpty(State.Player1) && !string.IsNullOrEmpty(State.Player2))
            {
                return Task.CompletedTask;
            }

            State.Player1 = player1;
            State.Player2 = player2;

            return this.WriteStateAsync();
        }

        async Task<MoveResponse> IGame.ProcessMove(MoveRequest move)
        {
            var actor = move.PlayerId == State.Player1 ? 0 : (move.PlayerId == State.Player2 ? 1 : -1);
            
            if (actor == -1) {
                return new MoveResponse {Result = TicTacToe.Result.WrongPlayer};
            }

            var response = new MoveResponse {
                Result = State.State.DoAction(actor, move.Move)
            };
            switch (response.Result)
            {
                case TicTacToe.Result.Finish:
                    await GrainFactory.Lobby().GameFinished((int) this.GetPrimaryKeyLong());
                    await this.WriteStateAsync();
                    break;
                case TicTacToe.Result.Update:
                    await this.WriteStateAsync();
                    break;
                default:
                    response.GameState = State.State;
                    break;
            }

            return response;
        }

        Task<MoveResponse> IGame.Check4Update(int turn)
        {
            if (State.State.Turn == turn)
            {
                return Task.FromResult(new MoveResponse {Result = TicTacToe.Result.NoUpdate});
            }
            
            return Task.FromResult(new MoveResponse
            {
                Result = TicTacToe.Result.Update,
                GameState = State.State
            });
        }
    }
}