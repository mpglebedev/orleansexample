using System.Collections.Generic;
using System.Threading.Tasks;
using HelloWorld.Interfaces;
using Orleans;
using Orleans.Providers;
using SharedLogic;

namespace HelloWorld.Grains
{
    public class OpenedGames
    {
        public class Game
        {
            public string Player1;
            public string Player2;
            public int GameId;

            public GameInfo Get(string playerId)
            {
                return new GameInfo
                {
                    GameId = GameId,
                    PlayerId = Player1 == playerId ? 0 : 1
                };
            }
        }

        public int GameCounter;
        public List<Game> Games = new List<Game>();
    } 
    
    [StorageProvider(ProviderName = "profileStore")]
    public class Lobby : Grain<OpenedGames>, ILobby
    {
        public async Task<GameInfo> JoinGame(string playerId)
        {
            var inProgress = State.Games.Find(g => g.Player1 == playerId || g.Player2 == playerId);

            if (inProgress == null) {
                inProgress = State.Games.Find(g => string.IsNullOrEmpty(g.Player2));
                if (inProgress != null) {
                    inProgress.Player2 = playerId;

                    await GrainFactory.Game(inProgress.GameId).Setup(inProgress.Player1, inProgress.Player2);
                    await this.WriteStateAsync();
                }
            }
            
            if (inProgress == null) {
                inProgress = new OpenedGames.Game {Player1 = playerId, GameId = State.GameCounter++};
                await GrainFactory.Game(inProgress.GameId).Setup(inProgress.Player1, "");
                State.Games.Add(inProgress);
                await this.WriteStateAsync();
            }
            
            return inProgress.Get(playerId);
        }

        Task ILobby.GameFinished(int gameId)
        {
            State.Games.RemoveAll(g => g.GameId == gameId);
            return this.WriteStateAsync();
        }
    }
}