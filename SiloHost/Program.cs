using System;
using System.Net;
using System.Threading.Tasks;
using HelloWorld.Grains;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;

namespace OrleansSiloHost
{
    public static class Program
    {
        public static Task Main(string[] args)
        {
            return new HostBuilder()
                .UseOrleans(builder =>
                {
                    builder
                        .ConfigureMongo()
                        .ConfigureCluster()
                        .Configure<EndpointOptions>(options => options.AdvertisedIPAddress = IPAddress.Loopback)
                        .ConfigureApplicationParts(parts => parts.AddApplicationPart(typeof(HelloGrain).Assembly).WithReferences())
                        .AddMemoryGrainStorage(name: "ArchiveStorage")
                        .ConfigureStorage("profileStore");
                })
                .ConfigureServices(services =>
                {
                    services.Configure<ConsoleLifetimeOptions>(options =>
                    {
                        options.SuppressStatusMessages = true;
                    });
                })
                .ConfigureLogging(builder =>
                {
                    builder.AddConsole();
                })
                .RunConsoleAsync();
        }

        static ISiloBuilder ConfigureMongo(this ISiloBuilder builder)
        {
            var mongoAddr= Environment.GetEnvironmentVariable("mongoConnect") ?? "mongodb://localhost:27017";
            
            System.Console.WriteLine($"Silo Mongo at {mongoAddr}");

            return builder
                .UseMongoDBClient(mongoAddr);
        }

        static ISiloBuilder ConfigureStorage(this ISiloBuilder builder, string storageName, string mongoDbEnv = "mongoDB")
        {
            var dbName = Environment.GetEnvironmentVariable(mongoDbEnv);
            if (dbName == null) {
                return builder.AddMemoryGrainStorage(name: storageName);
            }

            System.Console.WriteLine($"Storage {storageName} in Mongo Db: {dbName}");
            return builder
                .UseMongoDBReminders(options => { options.DatabaseName = dbName; })
                .AddMongoDBGrainStorage(storageName, options => {
                    options.DatabaseName = dbName;
                });
        }

        private static ISiloBuilder ConfigureCluster(this ISiloBuilder builder, string mongoDbEnv = "mongoDB")
        {
            var dbName = Environment.GetEnvironmentVariable(mongoDbEnv);
            if (dbName == null) {
                builder.UseLocalhostClustering();
            } else {
                System.Console.WriteLine($"Clustering in Mongo Db: {dbName}");
                builder.UseMongoDBClustering(options => { options.DatabaseName = dbName; });
            }

            return builder.Configure<ClusterOptions>(options => {
                options.ClusterId = "dev";
                options.ServiceId = "HelloWorldApp";
            });
        }
    }
}
