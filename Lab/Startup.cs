using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelloWorld.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using OrleansClient;
using SharedLogic;

namespace Lab {
    public class Startup {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services) { }

        static bool Deserialize<T>(string json, out T result)
        {
            try
            {
                result = Utf8Json.JsonSerializer.Deserialize<T>(json);
                return true;
            }
            catch (Exception e)
            {
                System.Console.Write(e);
                result = default(T);
                return false;
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints => {
                endpoints.MapGet("/", async context => {
                    var response = await ClusterClientHostedService.Client
                        .GetGrain<IHelloArchive>(0)
                        .SayHello("Hello!");

                    await context.Response.WriteAsync(response);
                });
                
                endpoints.MapGet("/set_name", async context => {

                    if (!context.Request.Query.TryGetValue("name", out var name)) {
                        name = "NoName";
                    }
                    
                    await context.Response.WriteAsync(await ClusterClientHostedService.Client
                        .GetGrain<IHelloArchive>(0)
                        .SetName(name));
                });
                
                endpoints.MapGet("/join", async context =>
                {
                    GameInfo result;
                    if (context.Request.Query.TryGetValue("data", out var playerId)
                        && Deserialize(playerId, out JoinRequest request))
                    {
                        result = await ClusterClientHostedService.Client.Lobby().JoinGame(request.PlayerId);
                    }
                    else
                    {
                        result = new GameInfo { GameId = -1 };
                    }
                    
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(result));
                });
                
                endpoints.MapGet("/move", async context =>
                {
                    MoveResponse result;
                    if (context.Request.Query.TryGetValue("data", out var playerId)
                        && Deserialize(playerId, out MoveRequest request))
                    {
                        result = await ClusterClientHostedService.Client.Game(request.GameId).ProcessMove(request);
                    }
                    else
                    {
                        result = new MoveResponse { Result = TicTacToe.Result.WrongAction };
                    }
                    
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(result));
                });
                
                endpoints.MapGet("/update", async context =>
                {
                    MoveResponse result;
                    if (context.Request.Query.TryGetValue("data", out var playerId)
                        && Deserialize(playerId, out UpdateRequest request))
                    {
                        result = await ClusterClientHostedService.Client.Game(request.GameId).Check4Update(request.Turn);
                    }
                    else
                    {
                        result = new MoveResponse { Result = TicTacToe.Result.WrongAction };
                    }
                    
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(result));
                });
            });
        }
    }
}