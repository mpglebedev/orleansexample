using System.Threading.Tasks;
using Orleans;
using SharedLogic;

namespace HelloWorld.Interfaces
{
    
    public class Game
    {
        public string Player1;
        public string Player2;
        public int GameId;

        public GameInfo Get(string playerId)
        {
            return new GameInfo
            {
                GameId = GameId,
                PlayerId = Player1 == playerId ? 0 : 1
            };
        }
    }

    public interface ILobby  : Orleans.IGrainWithIntegerKey
    {
        Task<GameInfo> JoinGame(string playerId);
        Task      GameFinished(int gameId);
    }

    public static partial class Ext
    {
        public static ILobby Lobby(this IGrainFactory factory) => factory.GetGrain<ILobby>(0);
    }
}