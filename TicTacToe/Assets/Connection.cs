﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SharedLogic;
using UnityEngine;
using UnityEngine.Networking;

public class Connection : MonoBehaviour
{
    public static Connection Instance;
    public string uri = "https://localhost:5001";

    private void Awake()
    {
        Instance = this;
    }

    public async Task<R> Do<T, R>(string method, T data)
    {
        try
        {
            return await DoImpl<T, R>(method, data);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }

        return default(R);
    }
    
    private async Task<R> DoImpl<T, R>(string method, T data)
    {
        UnityWebRequest www = UnityWebRequest.Get(uri+$"/{method}?data={JsonConvert.SerializeObject(data)}");
        www.certificateHandler = new CertHandler();
        www.SendWebRequest();

        while (!www.isDone) 
        {
            await Task.Delay(10);
        }

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.LogError(www.error);
            return default(R);
        }

        return JsonConvert.DeserializeObject<R>(www.downloadHandler.text);
    }
    
    //
    // Allow all https certificates
    //
    private class CertHandler : CertificateHandler
    {
        protected override bool ValidateCertificate(byte[] certificateData)
        {
            return true;
        }
    }

}
