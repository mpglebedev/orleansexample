﻿using System.Collections.Generic;
using SharedLogic;

public class TicTacToe
{
    public class FieldRect
    {
        private int x, y, width, height;

        public FieldRect(int x, int y, int w, int h)
        {
            this.x = x;
            this.y = y;
            width = w;
            height = h;
        }

        public bool Inside(int ix, int iy)
        {
            return x <= ix && ix <= x + width && y <= iy && iy <= y + height;
        }
        
    }

    public class TileInfo
    {
        public int owner;
        public int info;
        public Pos pos;
    }

    public class Pos
    {
        public Pos()
        {
        }

        public Pos(int x, int y)
        {
            X = x;
            Y = y;
        }
        public int X;
        public int Y;
    }
    
    public class State
    {
        public int Winner => _winner;
        
        public readonly FieldRect GameField = new FieldRect(-7, -7, 15, 15);
        public readonly int Size = 5;
        
        public int Turn;
        
        public Dictionary<string, TileInfo> Actions = new Dictionary<string, TileInfo>();
        public int _winner = -1;

        string Key(int x, int y) => $"{x}_{y}";

        private void SubDir(int x, int y, int ox, int oy, int owner, ref int length)
        {
            for (int i = 1; length < Size ; ++i, ++length)
            {
                if (!Actions.TryGetValue(Key(x + ox * i, y + oy * i), out var inf) || inf.owner != owner)
                    break;
            }
        }

        public bool Completed(int x, int y, int ox, int oy)
        {
            var l = 1;
            var owner = Actions[Key(x, y)].owner;

            SubDir(x, y, ox, oy, owner, ref l);
            SubDir(x, y, -ox, -oy, owner, ref l);
            return l >= Size;
        }
        
        public Result DoAction(int actor, PlayerMove playerMove)
        {
            var (x, y) = (playerMove.X, playerMove.Y);
            var pos = Key(x, y);
            if (_winner >= 0 || Actions.ContainsKey(pos) || Turn % 2 != actor || !GameField.Inside(x, y)) {
                return Result.WrongAction;
            }

            ++Turn; 

            Actions[pos] = new TileInfo
            {
                info = playerMove.ExtInfo, owner = actor, pos = new Pos(x, y)
            };

            if (Completed(x, y, 1, 0) || Completed(x, y, 0, 1)
                || Completed(x, y, 1, 1) || Completed(x, y, -1, 1)) {
                _winner = actor;
                return Result.Finish;
            }

            return Result.Update;
        }
    }

    public enum Result
    {
        WrongAction,
        Update,
        NoUpdate,
        Finish,
        WrongPlayer
    }

    public State GameState { get; set; }

    public TicTacToe()
    {
        GameState = new State();
    }
    public Result DoAction(int actor, PlayerMove action) => GameState.DoAction(actor, action);

}
