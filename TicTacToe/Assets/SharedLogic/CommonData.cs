namespace SharedLogic
{
    public class GameInfo
    {
        public int GameId;
        public int PlayerId;
    }

    public class JoinRequest
    {
        public JoinRequest()
        {
        }

        public JoinRequest(string playerId)
        {
            this.PlayerId = playerId;
        }

        public string PlayerId;
    }

    public class PlayerMove
    {
        public PlayerMove()
        {
        }

        public PlayerMove(int ix, int iy, int extInfo)
        {
            X = ix;
            Y = iy;
            ExtInfo = extInfo;
        }

        public int X;
        public int Y;
        public int ExtInfo;
    }

    public class MoveRequest
    {
        public PlayerMove Move;
        public int GameId;
        public string PlayerId;
    }

    public class UpdateRequest
    {
        public int GameId;
        public int Turn;
    }

    public class MoveResponse
    {
        public TicTacToe.Result Result;
        public TicTacToe.State GameState;
    }
}