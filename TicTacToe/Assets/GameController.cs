﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using SharedLogic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GameController : MonoBehaviour
{

    public static GameController Instance;
    public TicTacToe GameInfo = new TicTacToe();
    public Tilemap map;

    public List<TileBase> Player1;
    public List<TileBase> Player2;

    public List<GameObject> playerWin;
    
    private System.Random rnd = new System.Random();
    private int _gameId;
    private Dictionary<int, string> _players = new Dictionary<int, string>();

    private void Awake()
    {
        Instance = this;
    }

    public void Do(int actor, Vector3 action)
    {
        var pos = map.WorldToCell(action);
        var move = new PlayerMove(pos.x, pos.y, rnd.Next());
        switch (GameInfo.DoAction(actor, move))
        {
            case TicTacToe.Result.WrongAction:
                break;
            case TicTacToe.Result.Update:
            case TicTacToe.Result.Finish:
                UpdateAndSend(actor, move);
                break;
        }
    }


    async void UpdateAndSend(int actor, PlayerMove move)
    {
        UpdateVisual();

        var result = await Connection.Instance.Do<MoveRequest, MoveResponse>("move", new MoveRequest {
            Move = move,
            GameId = _gameId,
            PlayerId = _players[actor]
        });

        if (result.GameState != null)
        {
            GameInfo.GameState = result.GameState;
            UpdateVisual();
        }
    }

    public void UpdateVisual()
    {
        foreach (var gameStateAction in GameInfo.GameState.Actions)
        {
            var info = gameStateAction.Value;
            var pos = gameStateAction.Value.pos;
            var tilePos = new Vector3Int(pos.X, pos.Y, 0);

            var tiles = info.owner == 0 ? Player1 : Player2;
            map.SetTile(tilePos, tiles[info.info % tiles.Count]);
            
        }
    }

    async void GameUpdater(int gameId)
    {
        while (true)
        {

            await Task.Delay(1000);
            var result = await Connection.Instance.Do<UpdateRequest, MoveResponse>("update",
                new UpdateRequest {GameId = gameId, Turn = GameInfo.GameState.Turn});

            if (result.Result == TicTacToe.Result.Update && result.GameState != null)
            {
                GameInfo.GameState = result.GameState;
                UpdateVisual();
            }

            if (GameInfo.GameState.Winner >= 0)
            {
                playerWin[GameInfo.GameState.Winner].SetActive(true);
                return;
            }
        }
    }

    public void OnGameStarted(int actor, string playerId, int gameId)
    {
        _players[actor] = playerId;
        _gameId = gameId;

        GameUpdater(_gameId);
    }
}
