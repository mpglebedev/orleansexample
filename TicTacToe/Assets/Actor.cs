﻿using System;
using SharedLogic;
using UnityEngine;
using UnityEngine.Networking;

public class Actor : MonoBehaviour
{
    private int actor = -1;
    private string playerId;
    private GameInfo _gameInfo;


    public string forceId = "";
    
    private void Awake()
    {
        playerId = string.IsNullOrEmpty(forceId) ? PlayerPrefs.GetString("playerId", null) : forceId;
        if (string.IsNullOrEmpty(playerId)) {
            playerId = Guid.NewGuid().ToString();
            PlayerPrefs.SetString("playerId", playerId);
        }
    }

    void Start()
    {
        Join();
    }

    async void Join()
    {
        _gameInfo = await Connection.Instance.Do<JoinRequest, GameInfo>("join", new JoinRequest(playerId));
        
        if (_gameInfo != null)
        {
            actor = _gameInfo.PlayerId;
            GameController.Instance.OnGameStarted(actor, playerId, _gameInfo.GameId);
            Debug.Log($"Game {_gameInfo.GameId} ");
        }
    }

    void Update()
    {
        if (actor == -1) return;
        
        if (Input.GetMouseButtonDown(0)) {
            GameController.Instance.Do(actor, Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
    }
}
